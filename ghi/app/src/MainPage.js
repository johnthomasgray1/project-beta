

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className='col'>
        <div className='row'>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://cdn-icons-png.flaticon.com/128/2155/2155941.png" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Make Car Sale </p>
              <a href="sale/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://cdn1.iconfinder.com/data/icons/client-management/512/add-512.png" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Add Customer </p>
              <a href="customer/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://www.shareicon.net/download/2016/01/04/697959_people_512x512.png" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Add Sales Person </p>
              <a href="salesperson/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
        </div>
        <div className='col'>
        <div className='row'>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://static.vecteezy.com/system/resources/previews/020/376/377/non_2x/equipment-service-technician-line-icon-illustration-vector.jpg" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Add Technician </p>
              <a href="technician/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://t4.ftcdn.net/jpg/05/54/17/79/360_F_554177966_xX2acthwHjhZ9c8dTeLZZ7DhLjJJ0IRK.jpg" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Make Service Appointment </p>
              <a href="appointment/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
        </div>
        </div>
        <div className='col'>
        <div className='row'>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://static.vecteezy.com/system/resources/thumbnails/023/101/370/small/line-icon-for-manufactured-vector.jpg" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Add Manufacturer </p>
              <a href="manufacturer/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://t4.ftcdn.net/jpg/04/51/03/71/360_F_451037150_c0MZKDXYFGGerH4nkHoTzIA2sfj0dJEu.jpg" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Add Model </p>
              <a href="model/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
            <div className="col-lg-2 mx-auto">
            <div className="card" >
            <img src="https://cdn3.iconfinder.com/data/icons/transportation-vehicles-2/128/cars-inventory-512.png" width={10} height={100} className="card-img-top" alt="..."/>
            <div className="card-body">
              <p className="card-text">Register Car to Inventory </p>
              <a href="automobile/new" className="stretched-link"></a>
            </div>
            </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
